if ( document.querySelector( '[data-calculator]' ) ) {
	// debounce function
	var debounce = function ( func, wait, immediate ) {
		var timeout;
		return function () {
			var context = this, args = arguments;
			var later = function () {
				timeout = null;
				if ( !immediate ) func.apply( context, args );
			};
			var callNow = immediate && !timeout;
			clearTimeout( timeout );
			timeout = setTimeout( later, wait );
			if ( callNow ) func.apply( context, args );
		};
	};
	
	/**
	 * Calculator object
	 * @constructor
	 */
	var Calculator = function () {
	};
	
	Calculator.prototype = {
		
		// Ovde se definišu cene:
		prices: {
			t200 : 150,
			t500 : 125,
			t1000: 115,
			t2000: 110
		},
		
		// Veličina tabaka 330x660mm
		sheet: {
			width : 300,
			height: 595
		},
		
		// Inputs
		inputWidth : document.querySelector( '[data-length]' ),
		inputHeight: document.querySelector( '[data-height]' ),
		
		// Result fields
		price200 : document.querySelector( '[data-price="200"]' ),
		price500 : document.querySelector( '[data-price="500"]' ),
		price1000: document.querySelector( '[data-price="1000"]' ),
		price2000: document.querySelector( '[data-price="2000"]' ),
		
		// Parameters hold
		setWidth : 0,
		setHeight: 0,
		
		// calculator logic
		calculate: debounce( function ( calc ) {
			
			var perSheet = calc.quantityPerSheet( calc );
			
			// calculate all prices
			Object.keys( calc.prices ).forEach( function ( type ) {
				
				var numCopies = parseInt( type.slice( 1 ) );
				
				var totalSheets = Math.ceil( numCopies / perSheet );
				
				var sumPrice = totalSheets * calc.prices[ 't' + numCopies ];
				
				var copyPrice = sumPrice / numCopies;
				
				calc[ 'price' + numCopies ].innerHTML = ( copyPrice + 0.00001 ).toFixed( 2 );
				
			} );
			
		}, 500 ),
		
		// return quantity by better sheet orientation
		quantityPerSheet: function ( calc ) {
			
			var widthFirst = Math.floor( calc.sheet.width / calc.setWidth ) * Math.floor( calc.sheet.height / calc.setHeight );
			var heightFirst = Math.floor( calc.sheet.height / calc.setWidth ) * Math.floor( calc.sheet.width / calc.setHeight );
			
			return Math.max( widthFirst, heightFirst );
		},
		
		
		/**
		 * Event Listeners
		 */
		eventListeners: function ( calc ) {
			
			// Width input field
			calc.inputWidth.oninput = function () {
				calc.setWidth = parseInt( this.value ) + 2;
				if ( calc.setWidth && calc.setHeight ) {
					calc.calculate( calc );
				}
			};
			
			// Height input field
			calc.inputHeight.oninput = function () {
				calc.setHeight = parseInt( this.value ) + 2;
				if ( calc.setHeight && calc.setWidth ) {
					calc.calculate( calc );
				}
			}
			
		}
	};
	
	var calc = new Calculator();
	calc.eventListeners( calc );
}